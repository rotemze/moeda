import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Product} from './product';

@Component({
  selector: 'jce-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['product']
})
export class ProductComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Product>();

  product:Product;
  isEdit : boolean = false;
  editButtonText = 'Edit';

  // isPname:Boolean =true;
  // isCost:Boolean =true;
  // isCategoryId:Boolean =false;

  constructor() { }
  
  sendDelete(){
    this.deleteEvent.emit(this.product);
  }

  toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
    //  this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';    
  }

  // toggleShow(){
  //  this.isPname =true;
  // }

  ngOnInit() {
}

}
