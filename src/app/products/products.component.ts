import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';

@Component({
  selector: 'jce-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products;
  currentProduct;
  // isLoading:Boolean = true;


  deleteProduct(product){
    this._productsService.deleteProduct(product);
  }
  


  constructor(private _productsService:ProductsService) { }

  ngOnInit() {
    // this._productsService.getProducts().subscribe(products => this.products = products);
    this._productsService.getProducts().subscribe(productsDate =>{this.products = productsDate}) // like function(userData){this.usres=usersDats}
  }

}
