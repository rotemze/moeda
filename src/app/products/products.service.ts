import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';

@Injectable()
export class ProductsService {

  productsObservable;

  constructor(private af:AngularFire) { }

  getProducts(){
		// return this._http.get(this._url).map(res => res.json());
    this.productsObservable = this.af.database.list('/products');
    return this.productsObservable;
	}
  getProductsWithName(){
    this.productsObservable = this.af.database.list('/products').map(
      products => {
        products.map(
          product => {
          product.categoryNames =[];
            product.categoryNames.push(this.af.database.object('/category/' + product.categoryId)
            )
          }
        
    );
    return products;
  }
    )
    return this.productsObservable;
	}
  //  getPosts(){

  //   this.postsObservable = this.af.database.list('/posts').map(
  //     posts => {
  //       posts.map(
  //         post => {
  //           post.userNames = [];
  //           for(var p in post.users){
  //             post.userNames.push(
  //               this.af.database.object('/users/' + p)
  //             )
  //           }
  //         }
  //       );
  //       return posts;
  //     }
  //   )
  //   return this.postsObservable;
  // }

  deleteProduct(product){
    let productKey = product.$key;
    this.af.database.object('/products/' + productKey).remove();
  }

}
