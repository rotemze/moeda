import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products/products.service';

@Component({
  selector: 'jce-product2',
  templateUrl: './product2.component.html',
  styleUrls: ['./product2.component.css']
})
export class Product2Component implements OnInit {

  products;

  constructor(private _productsService:ProductsService) { }

  ngOnInit() {
    this._productsService.getProductsWithName().subscribe(productsDate =>{this.products = productsDate})
  }

}
